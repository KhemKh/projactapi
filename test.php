<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/logo-icon-api.png">

    <title>Sharing Thai Food</title>

    <!-- Bootstrap core CSS -->
    <link href="mainstyle/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="mainstyle/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="mainstyle/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet"
        type="text/css">

    <!-- Custom styles for this template -->
    <link href="css/landing-page.min.css" rel="stylesheet">

    <link href="css/all.min.css" rel="stylesheet">

</head>

<body>

    <!-- Navigation -->
    <?php include("function/navigation.php"); ?>

    <!-- Masthead -->
    <?php include("function/search.php"); ?>

    <!-- Icons Grid -->
    <form action="show-manu.php" method="get">
        <?php include("function/region.php"); ?>

    </form>



    <section class="container">
        <div class="row">
            <div class="col-9">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-4 m">
                                <center>
                                    <img class="card-img-left"
                                        src="https://image.freepik.com/free-vector/good-food-logo-template_79169-17.jpg"
                                        width="250" height="250" alt="Card image cap">
                                </center>
                            </div>
                            <div class="col-8">
                                <h5 class="card-title">อาหาร</h5>
                                <p class="card-text">อาหารอาหารอาหารอาหารอาหาร</p>
                                <br>
                                <h5 class="card-title">วัตถุดิบ</h5>

                                <div class="row">
                                    <div class="col-8">
                                        <p> เกลือ</p>
                                    </div>
                                    <div class="col-2">
                                        <p class="ml-5">1</p>
                                    </div>
                                    <div class="col-2">
                                        <p>g</p>
                                    </div>
                                    <div class="col-8">
                                        <p> เกลือ</p>
                                    </div>
                                    <div class="col-2">
                                        <p class="ml-5">1</p>
                                    </div>
                                    <div class="col-2">
                                        <p>g</p>
                                    </div>
                                    <div class="col-8">
                                        <p> เกลือ</p>
                                    </div>
                                    <div class="col-2">
                                        <p class="ml-5">1</p>
                                    </div>
                                    <div class="col-2">
                                        <p>g</p>
                                    </div>
                                    <div class="col-8">
                                        <p> เกลือ</p>
                                    </div>
                                    <div class="col-2">
                                        <p class="ml-5">1</p>
                                    </div>
                                    <div class="col-2">
                                        <p>g</p>
                                    </div>
                                </div>


                                <br>
                                <h5 class="card-title">ขั้นตอนการทำ</h5>

                                <div class="row">
                                    <div class="col-1">
                                        <p>1.</p>
                                    </div>
                                    <div class="col-11">
                                        <p> บลาบลาบลาบลาบลาบลาบลา</p>
                                    </div>
                                    <div class="col-1">
                                        <p>1.</p>
                                    </div>
                                    <div class="col-11">
                                        <p> บลาบลาบลาบลาบลาบลาบลา</p>
                                    </div>
                                    <div class="col-1">
                                        <p>1.</p>
                                    </div>
                                    <div class="col-11">
                                        <p> บลาบลาบลาบลาบลาบลาบลา</p>
                                    </div>
                                    <div class="col-1">
                                        <p>1.</p>
                                    </div>
                                    <div class="col-11">
                                        <p> บลาบลาบลาบลาบลาบลาบลา</p>
                                    </div>
                                    <div class="col-1">
                                        <p>1.</p>
                                    </div>
                                    <div class="col-11">
                                        <p> บลาบลาบลาบลาบลาบลาบลา</p>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-3">
                <div class="card">
                    <div class="card-body">
                        <center>
                            <h5 class="card-title">อาหารแนะนำ</h5>
                        </center>
                        <div class="row">
                            <div class="col"><img class="card-img-left"
                                    src="https://image.freepik.com/free-vector/good-food-logo-template_79169-17.jpg"
                                    width="100" height="100" alt="Card image cap">
                            </div>

                            <div class="col">บลาบลาบลาบลาบลาบลา<a class="link" href="show-food.php?id=<?= $rowmenu1["id"]; ?>">Read more</a></div>
                        </div>
                        <div class="row">
                            <div class="col"><img class="card-img-left"
                                    src="https://image.freepik.com/free-vector/good-food-logo-template_79169-17.jpg"
                                    width="100" height="100" alt="Card image cap">
                            </div>

                            <div class="col">บลาบลาบลาบลาบลาบลา<a class="link" href="show-food.php?id=<?= $rowmenu1["id"]; ?>">Read more</a></div>
                        </div>   <div class="row">
                            <div class="col"><img class="card-img-left"
                                    src="https://image.freepik.com/free-vector/good-food-logo-template_79169-17.jpg"
                                    width="100" height="100" alt="Card image cap">
                            </div>

                            <div class="col">บลาบลาบลาบลาบลาบลา<a class="link" href="show-food.php?id=<?= $rowmenu1["id"]; ?>">Read more</a></div>
                        </div>   <div class="row">
                            <div class="col"><img class="card-img-left"
                                    src="https://image.freepik.com/free-vector/good-food-logo-template_79169-17.jpg"
                                    width="100" height="100" alt="Card image cap">
                            </div>

                            <div class="col">บลาบลาบลาบลาบลาบลา<a class="link" href="show-food.php?id=<?= $rowmenu1["id"]; ?>">Read more</a></div>
                        </div>





                    </div>
                </div>
            </div>
        </div>



    </section>

    <!-- BMR Calculator , Congenital disease , Food allergies -->
    <?php include("function/another-function.php"); ?>

    <!-- Call to Action -->
    <section class="call-to-action text-white text-center"
        style="background-image: url('http://www.healthyhome.asia/wp-content/uploads/2017/11/1-2-2.jpg') ;">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-xl-9 mx-auto">
                    <h2 class="mb-4">Find your favorite food</h2>
                </div>
                <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
                    <form action="#">
                        <div class="form-row input-group">
                            <div class="col-12 col-md-9 mb-2 mb-md-0">
                                <input type="text" name="search" class="form-control form-control-lg"
                                    placeholder="Searching for...">
                            </div>
                            <div class="col-12 col-md-3">
                                <button type="submit" class="btn btn-block btn-lg btn-primary">Searching!</button>
                                <a href='#top' id="">Go Top</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
    </section>

    <!-- Footer -->
    <footer class="footer bg-light">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
                    <ul class="list-inline mb-2">
                        <li class="list-inline-item">
                            <a href="#">About</a>
                        </li>
                        <li class="list-inline-item">&sdot;</li>
                        <li class="list-inline-item">
                            <a href="#">Contact</a>
                        </li>
                        <li class="list-inline-item">&sdot;</li>
                        <li class="list-inline-item">
                            <a href="#">Terms of Use</a>
                        </li>
                        <li class="list-inline-item">&sdot;</li>
                        <li class="list-inline-item">
                            <a href="#">Privacy Policy</a>
                        </li>
                    </ul>
                    <p class="text-muted small mb-4 mb-lg-0">&copy; Your Website 2019. All Rights Reserved.</p>
                </div>
                <div class="col-lg-6 h-100 text-center text-lg-right my-auto">
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item mr-3">
                            <a href="#">
                                <i class="fab fa-facebook fa-2x fa-fw"></i>
                            </a>
                        </li>
                        <li class="list-inline-item mr-3">
                            <a href="#">
                                <i class="fab fa-twitter-square fa-2x fa-fw"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">
                                <i class="fab fa-instagram fa-2x fa-fw"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>



    <!-- On top -->
    <div class="secondmenu text-right">
        <a href='#top' id="">
            <i class="fa fa-angle-up btn btn-block btn-lg " style="width: 50px; height: 43px;" aria-hidden="true"></i>
        </a>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="mainstyle/jquery/jquery.min.js"></script>
    <script src="mainstyle/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script>
        // On top
        $("a[href='#top']").click(function () {
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
            return false;
        });
    </script>
</body>

</html>