<aside class="left-sidebar">
    <div class="scroll-sidebar">
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li> <a class="waves-effect waves-dark" href="dashboard.php" aria-expanded="false">
                        <i class="fa fa-tachometer" aria-hidden="true"></i><span class="hide-menu">Dashboard</span>
                    </a>
                </li>
                <li>
                    <a class="waves-effect waves-dark" href="profile.php" aria-expanded="false">
                        <i class="fa fa-user-circle-o"></i><span class="hide-menu">Profile</span>
                    </a>
                </li>
                <li> <a class="waves-effect waves-dark" href="main-food.php" aria-expanded="false">
                        <i class="fa fa-cutlery" aria-hidden="true"></i><span class="hide-menu">Main Food</span>
                    </a>
                </li>
                <li> <a class="waves-effect waves-dark" href="bmr-calculator.php" aria-expanded="false">
                        <i class="fa fa-calculator" aria-hidden="true"></i><span class="hide-menu">BMR Calculator</span>
                    </a>
                </li>
                <li> <a class="waves-effect waves-dark" href="medical-problems.php" aria-expanded="false">
                        <i class="fa fa-medkit" aria-hidden="true"></i><span class="hide-menu">Medical problems</span>
                    </a>
                </li>
                <li> <a class="waves-effect waves-dark" href="food-allergies.php" aria-expanded="false">
                        <i class="fa fa-ambulance" aria-hidden="true"></i><span class="hide-menu">Food allergies</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>