<aside class="left-sidebar">
    <div class="scroll-sidebar">
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li> <a class="waves-effect waves-dark" href="admindashboard.php" aria-expanded="false">
                        <i class="fa fa-tachometer" aria-hidden="true"></i><span class="hide-menu">Dashboard</span>
                    </a>
                </li>
                <li>
                    <a class="waves-effect waves-dark" href="processuser.php" aria-expanded="false">
                        <i class="fa fa-user-circle-o"></i><span class="hide-menu">User</span>
                    </a>
                </li>
                <li> <a class="waves-effect waves-dark" href="addfood-admin.php" aria-expanded="false">
                        <i class="fa fa-cutlery text-danger" aria-hidden="true"></i><span class="hide-menu text-danger">Add Food</span>
                    </a>
                </li>
                <li> <a class="waves-effect waves-dark" href="allfood.php" aria-expanded="false">
                        <i class="fa fa-cutlery" aria-hidden="true"></i><span class="hide-menu">Food</span>
                    </a>
                </li>
                <li> <a class="waves-effect waves-dark" href="data-bmr-calculator.php" aria-expanded="false">
                        <i class="fa fa-calculator" aria-hidden="true"></i><span class="hide-menu">BMR Calculator</span>
                    </a>
                </li>
                <li> <a class="waves-effect waves-dark" href="data-medical-problems.php" aria-expanded="false">
                        <i class="fa fa-medkit" aria-hidden="true"></i><span class="hide-menu">Medical problems</span>
                    </a>
                </li>
                <li> <a class="waves-effect waves-dark" href="data-food-allergies.php" aria-expanded="false">
                        <i class="fa fa-ambulance" aria-hidden="true"></i><span class="hide-menu">Food allergies</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>